<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('product','ProductController@Index')->name('Product_page');
Route::post('add_product','ProductController@AddProduct')->name('add_product_page');
Route::post('currency_name','ProductController@CurrencyName');