<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Product;
use GuzzleHttp\Client;

class ProductController extends Controller
{
    public function Index(){
        $client = new Client();
        $Product_Data = Product::all();
        $request= $client->get('https://restcountries.eu/rest/v2/all');
        $country_data = json_decode($request->getBody());
        return view('product_view')->with(['data'=>$country_data,'pro_data'=>$Product_Data]);;
    }

    public function AddProduct(Request $r){
        $Add_Data = new Product;
        $Add_Data->product_name = $r->product_name;
        $Add_Data->product_desc = $r->product_desc;
        $Add_Data->month_name = $r->month;
        $Add_Data->country_name = $r->country_name;
        $Add_Data->currency = $r->currency;
        $Add_Data->save();
        return redirect()->route('Product_page')->with(['success'=>true,'message'=>'Add successfully!']);
    }

    public function CurrencyName(Request $r){
        
        $html = '<label for="pwd">Country Currency</label>
        <input type="text" class="form-control currency" name="currency" value="'.$r->currency.'">';
        return response()->json(array('html'=> $html), 200);
    }
}
