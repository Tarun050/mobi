<!DOCTYPE html>
<html lang="en">
<head>
<meta name="csrf-token" content="{{ csrf_token() }}" />

  <title>Bootstrap Example</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
  <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/2.1.4/toastr.min.css">
</head>
<body>

<div class="container">
  <!-- Trigger the modal with a button -->
  <button type="button" class="btn btn-info btn-lg" data-toggle="modal" data-target="#myModal">Add Product</button>

  <!-- Modal -->
  <div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Modal Header</h4>
        </div>
        <div class="modal-body">
        <form action="{{ route('add_product_page')}}" method="POST">
            @csrf
            <div class="form-group">
                <label for="product_name">Product Name</label>
                <input type="text" class="form-control" id="product_name" placeholder="Product Name" name="product_name">
            </div>

            <div class="form-group">
                <label for="product_desc">Product Descritpion</label><br>
                <textarea id="product_desc" name="product_desc" rows="4" class="form-control" placeholder="Product Description"></textarea>
            </div>

            <div class="input-group date form-group" data-provide="datepicker">
                <label for="month">Month:</label>
                <input type="date" id="month" name="month">
            </div>

            <div class="form-group">
              <label class="form-control-label" for="input-email">Country Name*</label>
              <select class="form-control country_name" name="country_name"
                  value="{{ old('is_active') }}">
                  <option value="">Please Select Country Name</option>
                  @foreach($data as $Keys=>$country_data)
                  <option value="{{ $country_data->name }}" data-currency="{{ $country_data->currencies[0]->name}}">{{ $country_data->name }}</option>
                  @endforeach
              </select>
            </div>

            <div class="form-group currency">
                
            </div>
            <button type="submit" class="btn btn-success">Submit</button>
        </form>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
        </div>
      </div>
      
    </div>

    
  </div>
  <table class="table">
    <thead>
      <tr>
        <th>Product Name</th>
        <th>Product Description</th>
        <th>Month Name</th>
        <th>Country Name</th>
        <th>Currency</th>
      </tr>
    </thead>
    <tbody>
      @foreach($pro_data as $value)
      <tr>
        <td>{{ $value->product_name }}</td>
        <td>{{ $value->product_desc }}</td>
        <td>{{ date('F', strtotime($value->month_name)) }}</td>
        <td>{{ $value->country_name }}</td>
        <td>{{ $value->currency }}</td>
      </tr>
      @endforeach
    </tbody>
  </table>
  
</div>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/2.1.4/toastr.min.js"></script>
<script>
$(".country_name").on('change', function(){
  var country_currency = $(this).find(':selected').data('currency');
  if (country_currency != "") {
    console.log('hi');
    $.ajax({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        type: 'POST',
        url: 'http://127.0.0.1:8000/'+'currency_name',
        data: {
            "currency": country_currency
        },
        async: true,
        crossDomain: true,
        dataType : "json",
        success: function(data) {
            $(".currency").html(data.html);
        }
    });
  }
})
@if(Session::has('message'))
    var type = "{{ Session::get('alert-type', 'info') }}";
    switch(type){
        case 'info':
            toastr.info("{{ Session::get('message') }}");
            break;

        case 'warning':
            toastr.warning("{{ Session::get('message') }}");
            break;

        case 'success':
            toastr.success("{{ Session::get('message') }}");
            break;

        case 'error':
            toastr.error("{{ Session::get('message') }}");
            break;
    }
  @endif
</script>
</body>
</html>
